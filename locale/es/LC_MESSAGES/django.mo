��          �      �        1  	  �  ;  �   �  
   �     �     �     �       
     
   $  	   /     9     I  	   g     q     �     �     �     �  3   �  4   �  !     �  9  g   �  �  8	    �
               6  %   Q     w  
   �     �     �  3   �     �          
     %     -     1     D  ;   M  1   �  !   �                                          
   	                                                           
            If you would like to be notified when The Functionary is indeed
            functional, please complete the form below. We do solemnly swear to
            only use said information to contact you about our launch. Anything
            else would be quite uncivilized. Thank you.
             
            The Functionary, when fully operational, will manage any and all
            back-office functions for your company. Leaving you to focus on what
            our Chief Jargon Officer refers to as &ldquo;core competencies.&rdquo;
            Honestly, we’re of the mind that you are undoubtedly much more than
            merely competent at your desired business activities, but
            alliteration rules the day.
             
          If you're reading this, you are either in desperate need of help for
          your business, or with your web browsing skills. Let us, for a moment,
          assume it's the former.
           Accounting All Rights Reserved Application Development Call Center Email Address Facilities First Name Help Desk Human Resources Integration &amp; Warehousing Last Name Marketing &amp; Advertising Message NOC Our Functions Submit Thank you for reaching out. We'll be in touch soon! This junction for your functions is almost complete. Whatever you aren&apos;t, we are. Project-Id-Version: 1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-06-17 04:52+0000
PO-Revision-Date: 2017-07-02 11:49-0500
Last-Translator: Ben Davis <ben@bendavisdigital.com>
Language-Team: English <ben@bendavisdigital.com>
Language: ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.7
 
            Si te gustaría escuchar más acerca de The Functioary por favor
            contáctanos. 
            The Functionary, podrá manejar todas y cada una de las funciones de 
            back-office para tu empresa dejando que te enfoques en tus "ventajas 
            competitivas" y no tengas que preocuparte del resto. Nosotros te 
            ayudaremos a que tengas una mayor rentabilidad y contar con un mejor 
            control en áreas de soporte y así, poder ver crecer tu negocio. Sólo 
            déjalo en nuestra manos. 
          Si estas leyendo esto es porque probablemente estas buscando ayuda
          para tú negocio ó porque tienes muy buenas habilidades para utilizar
          un buscador. Deja que asumamos que es lo primero y te digamos como
          podemos ayudarte.
           Contabilidad Todos los Derechos Reservados Desarrollo de aplicaciones Manejo y negociación con proveedores Dirección Email Facilities Nombre Servicio al Cliente Administración de Recursos Humanos y Reclutamiento Compras y manejo de inventario Apellido Mercadotecnia y publicidad Mensaje NOC Nuestras Funciones Ingresar Gracias por contactarnos. Estaremos en contacto muy pronto! Esta fusión para tus funciones está casi lista. Whatever you aren&apos;t, we are. 