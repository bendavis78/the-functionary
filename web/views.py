from django.shortcuts import render
from webmessages.forms import MessageForm


def index(request):
    context = {'success': False}
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            form.save()
            context['success'] = True
    else:
        form = MessageForm()

    context['form'] = form

    return render(request, 'web/index.html', context)

# Create your views here.
